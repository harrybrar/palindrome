package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invlaid Input", Palindrome.isPalindrome("Anna"));
	}
	
	@Test
	public void testIsNotPalindrome() {
		assertFalse("Invlaid Input", Palindrome.isPalindrome("Goal"));
	}
	
	@Test
	public void testIsClosePalindrome() {
		assertTrue("Invlaid Input", Palindrome.isPalindrome("a"));
	}
	
	@Test
	public void testIsAlmostPalindrome() {
		assertFalse("Invlaid Input", Palindrome.isPalindrome("allas"));
	}
	

}
