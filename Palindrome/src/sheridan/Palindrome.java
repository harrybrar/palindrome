package sheridan;

public class Palindrome {

	public static boolean isPalindrome(String text) {
		text = text.toLowerCase().replaceAll(" ", "");
		
		for(int i = 0, j = (text.length() - 1); i < j; i++, j--) {
			if(text.charAt(i) != text.charAt(j)) {
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Is Anna Plalindrome?" + Palindrome.isPalindrome("anna"));
		System.out.println("Is Race Car Plalindrome?" + Palindrome.isPalindrome("race car"));
		System.out.println("Is Taco Cat Plalindrome?" + Palindrome.isPalindrome("taco cat"));
		System.out.println("Is stuff Plalindrome?" + Palindrome.isPalindrome("stuff"));
	}

}
